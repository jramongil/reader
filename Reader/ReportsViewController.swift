//
//  ReportsViewController.swift
//  Reader
//
//  Created by Javier Ramon Gil on 9/5/15.
//  Copyright (c) 2015 Apress. All rights reserved.
//

import UIKit

class ReportsViewController: UIViewController {
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var bodyView: UITextView!
    var headerSegue: String!
    var bodiesSegue: String!
    let favoritesList = FavoritesList.sharedFavoriteList
    let favButton: UIButton = UIButton()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        navigationController?.navigationBar.barTintColor = UIColor.redColor()
        navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        navigationController?.navigationBar.topItem?.title = "Atrás"
        
        favButton.setImage(UIImage(named: "favorite_default"), forState: .Normal)
        favButton.frame = CGRectMake(0, 0, 45, 45)
        
        var rightItem: UIBarButtonItem = UIBarButtonItem()
        rightItem.customView = favButton
        self.navigationItem.rightBarButtonItem = rightItem
        self.navigationItem.rightBarButtonItem?.tintColor = UIColor.whiteColor()
        favButton.addTarget(self, action: "favButtonPressed", forControlEvents: UIControlEvents.TouchUpInside)
        
        headerLabel.text = headerSegue
        headerLabel.textColor = UIColor.redColor()
        headerLabel.sizeToFit()
        bodyView.text = bodiesSegue
        bodyView.textColor = UIColor.grayColor()
        bodyView.editable = false
    }
    
    func favButtonPressed(){
        favoritesList.addFavorite(headerSegue)
        
        if favButton.state == UIControlState.Highlighted {
            favButton.setImage(UIImage(named: "favorite_selected"), forState: .Highlighted)
        } else {
            favButton.setImage(UIImage(named: "favorite_default"), forState: .Normal)
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
