//
//  FavoritesViewController.swift
//  Reader
//
//  Created by Javier Ramon Gil on 4/6/15.
//  Copyright (c) 2015 Apress. All rights reserved.
//

import UIKit

class FavoritesViewController: UIViewController {
    @IBOutlet weak var headerFavLabel: UILabel!
    @IBOutlet weak var bodyFavView: UITextView!
    var favHeader: String!
    var favBody: String!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        navigationController?.navigationBar.barTintColor = UIColor.redColor()
        navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        navigationController?.navigationBar.topItem?.title = "Atrás"
        
        headerFavLabel.text = favHeader
        headerFavLabel.textColor = UIColor.redColor()
        headerFavLabel.sizeToFit()
        bodyFavView.text = favBody
        bodyFavView.textColor = UIColor.grayColor()
        bodyFavView.editable = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
