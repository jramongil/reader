//
//  FavoritesList.swift
//  Reader
//
//  Created by Javier Ramon Gil on 23/5/15.
//  Copyright (c) 2015 Apress. All rights reserved.
//

import Foundation
import UIKit

class FavoritesList {
    class var sharedFavoriteList : FavoritesList {
        struct Singleton {
            static let instance = FavoritesList()
        }
        return Singleton.instance;
    }
    
    private(set) var favorites:[String]
    
    init() {
        let defaults = NSUserDefaults.standardUserDefaults()
        let storedFavorites = defaults.objectForKey("favorites") as? [String]
        favorites = storedFavorites != nil ? storedFavorites! : []
    }
    func addFavorite(header: String) {
        if (!contains(favorites, header)) {
        favorites.append(header)
        saveFavorites()
        }
    }
    func removeFavorite(header: String) {
        if let index = find(favorites, header) {
        favorites.removeAtIndex(index)
        saveFavorites()
        }
    }
    private func saveFavorites() {
        let defaults = NSUserDefaults.standardUserDefaults()
        defaults.setObject(favorites, forKey: "favorites")
        defaults.synchronize()
    }
}