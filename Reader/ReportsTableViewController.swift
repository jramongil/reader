//
//  ReportsTableViewController.swift
//  Reader
//
//  Created by Javier Ramon Gil on 7/5/15.
//  Copyright (c) 2015 Apress. All rights reserved.
//

import UIKit

class ReportsTableViewController: UITableViewController, ReportsTableViewCellDelegate {
    @IBOutlet weak var titleHeader: UINavigationItem!
    let cellTableIdentifier = "CellTableIdentifier"
    var headers: [String]!
    var headerSegue: String!
    var bodies: [String]!
    var bodiesSegue: String!
    var identifiers: [String]!
    
    var report: Report!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        titleHeader.titleView = UIImageView(image: UIImage(named: "navbar_title"))
        navigationController?.navigationBar.barTintColor = UIColor.redColor()
        
        tableView.registerClass(ReportsTableViewCell.self, forCellReuseIdentifier: cellTableIdentifier)
        
        let nib = UINib(nibName: "ReportsTableViewCell", bundle: nil)
        tableView.registerNib(nib, forCellReuseIdentifier: cellTableIdentifier)
        
        let path = NSBundle.mainBundle().pathForResource("Reports", ofType: "plist")
        let reportsArray = NSMutableArray(contentsOfFile: path!)
        headers = reportsArray?.valueForKey("header") as! [String]
        bodies = reportsArray?.valueForKey("body") as! [String]
        identifiers = reportsArray?.valueForKey("identifier") as! [String]
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        return headers.count
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier(cellTableIdentifier, forIndexPath: indexPath) as! ReportsTableViewCell
        
        headerSegue = headers[indexPath.row]
        bodiesSegue = bodies[indexPath.row]
        
        cell.title = headers[indexPath.row]
        cell.delegate = self
        
        return cell
        
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        performSegueWithIdentifier("ReportsSegue", sender: nil)
    }
    
    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
        let reportsVC = segue.destinationViewController as! ReportsViewController
        let indexPath = tableView.indexPathForSelectedRow()?.row
        reportsVC.headerSegue = headers[indexPath!]
        reportsVC.bodiesSegue = bodies[indexPath!]
    }
  

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the item to be re-orderable.
        return true
    }
    */

    
    // MARK - ReportsTableViewCellDelegate
    
    func reportsTableViewCell(cell: ReportsTableViewCell, didChangeValue value: Bool) {
        let indexPath = tableView.indexPathForCell(cell)
        // TODO - Coninue implementation
        //println("Index: \(indexPath!.row) - status: \(value)")
        
        let favoritesList = FavoritesList.sharedFavoriteList
        favoritesList.addFavorite(headers[indexPath!.row])
        
       
    }
}
