//
//  ReportsTableViewCell.swift
//  Reader
//
//  Created by Javier Ramon Gil on 25/5/15.
//  Copyright (c) 2015 Apress. All rights reserved.
//

import UIKit

protocol ReportsTableViewCellDelegate {
    func reportsTableViewCell(cell: ReportsTableViewCell, didChangeValue value: Bool)
}

class ReportsTableViewCell: UITableViewCell {
    @IBOutlet weak var favButton: UIButton!
    @IBOutlet weak var headerLabel: UILabel!
    
    var delegate: ReportsTableViewCellDelegate?
    
    var title: String = "" {
        didSet {
            if title != oldValue {
                headerLabel.text = title
            }
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        
    }
    
    @IBAction func favButtonPressed(sender: UIButton){
        
        delegate?.reportsTableViewCell(self, didChangeValue: sender.selected)
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
