//
//  FavoritesTableViewController.swift
//  Reader
//
//  Created by Javier Ramon Gil on 7/5/15.
//  Copyright (c) 2015 Apress. All rights reserved.
//

import UIKit

class FavoritesTableViewController: UITableViewController, FavoritesTableViewCellDelegate {
    @IBOutlet weak var titleHeader: UINavigationItem!
    let cellTableIdentifier = "CellTableIdentifier"
    let favoritesList = FavoritesList.sharedFavoriteList
    var headers: [String]!
    var bodies: [String]!
    var favHeaderSegue: String!
    var favBodySegue: String!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        titleHeader.titleView = UIImageView(image: UIImage(named: "navbar_title"))
        navigationController?.navigationBar.barTintColor = UIColor.redColor()
        
        tableView.registerClass(FavoritesTableViewCell.self, forCellReuseIdentifier: cellTableIdentifier)
        
        let nib =  UINib(nibName: "FavoritesTableViewCell", bundle: nil)
        tableView.registerNib(nib, forCellReuseIdentifier: cellTableIdentifier)
        
        let path = NSBundle.mainBundle().pathForResource("Reports", ofType: "plist")
        let reportsArray = NSMutableArray(contentsOfFile: path!)
        headers = reportsArray?.valueForKey("header") as! [String]
        bodies = reportsArray?.valueForKey("body") as! [String]
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        tableView.reloadData()
    }

    // MARK: - Table view data source

    /*override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 0
    }*/

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        return favoritesList.favorites.count
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(cellTableIdentifier, forIndexPath: indexPath) as! FavoritesTableViewCell

        // Configure the cell...
        cell.favLabel.text = favoritesList.favorites[indexPath.row]

        cell.delegate = self

        return cell
    }
    
    func favoritesTableViewCell(cell: FavoritesTableViewCell) {
        let indexPath = tableView.indexPathForCell(cell)
        
        let favoritesList = FavoritesList.sharedFavoriteList
        favoritesList.removeFavorite(favoritesList.favorites[indexPath!.row])
        tableView.reloadData()
        
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        favHeaderSegue = favoritesList.favorites[indexPath.row]
        let index = find(headers, favHeaderSegue)
        favBodySegue = bodies[index!]
        performSegueWithIdentifier("FavsSegue", sender: nil)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let favoritesVC = segue.destinationViewController as! FavoritesViewController
        let indexPath = tableView.indexPathForSelectedRow()?.row
        
        favoritesVC.favHeader = favHeaderSegue
        favoritesVC.favBody = favBodySegue
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the item to be re-orderable.
        return true
    }
    */


}
