//
//  FavoritesTableViewCell.swift
//  Reader
//
//  Created by Javier Ramon Gil on 2/6/15.
//  Copyright (c) 2015 Apress. All rights reserved.
//

import UIKit

protocol FavoritesTableViewCellDelegate{
    func favoritesTableViewCell(cell: FavoritesTableViewCell)
}

class FavoritesTableViewCell: UITableViewCell {
    @IBOutlet var favLabel: UILabel!
    
    var delegate: FavoritesTableViewCellDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func favRemove(sender: AnyObject) {
        delegate?.favoritesTableViewCell(self)
    }
    

}
